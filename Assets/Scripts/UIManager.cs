﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TTT
{
    public class UIManager : MonoBehaviour
    {
        //Variables
        public static UIManager Instance;

        TTTSession curSession;
        TTTGame curGame;

        [Header("Text Fields")]
        public Text curTurnText;
        public Text xScoreText;
        public Text oScoreText;
        public Text popUpResultText;

        [Space]
        [Header("Buttons")]
        public Button endSessionButton;
        public Button playAgainButton;

        [Space]
        [Header("Popups")]
        public GameObject endGamePopUpPanel;
      
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        void Start()
        {
            GameManager.Instance.OnSessionStarted += SessionStarted;
            GameManager.Instance.OnSessionEnded += SessionEnded;

            endGamePopUpPanel.gameObject.SetActive(false);
        }

        public void SessionStarted(TTTSession newSession)
        {
            curSession = newSession;
            curSession.GameStarted += NewGameStarted;
            curSession.GameEnded += GameEnded;

            playAgainButton.onClick.AddListener(curSession.NewGame);

            oScoreText.text = xScoreText.text = "0";
        }

        public void SessionEnded(TTTSession sessionThatEnded)
        {
            playAgainButton.onClick.RemoveListener(curSession.NewGame);
        }

        public void NewGameStarted(TTTGame newGame)
        {
            curGame = newGame;
            curGame.NewRound += NewRoundStarted;
            NewRoundStarted(curGame.PlayerPlaying);
        }

        public void GameEnded(TTTGame gameThatEnded)
        {
            int xScore, oScore;
            curSession.GetScore(out xScore, out oScore);

            xScoreText.text = xScore.ToString();
            oScoreText.text = oScore.ToString();

            popUpResultText.text = curGame.GameState == GameState.XWon ? "X Won" : curGame.GameState == GameState.OWon ? "O Won" : "Draw";
            endGamePopUpPanel.gameObject.SetActive(true);
        }
        public void SessionEnded(TTTGame gameThatEnded)
        {

        }

        public void NewRoundStarted(Player curPlayer)
        {
            if (curGame.GameMode == GameMode.PvP)
            {
                curTurnText.text = (curPlayer == Player.X_Player ? "X" : "O") + " Player's Turn";
            }
            else
            {
                curTurnText.text = (curPlayer == Player.X_Player ? "Your" : "Opponent") + " Turn";
            }
        }
    }
}