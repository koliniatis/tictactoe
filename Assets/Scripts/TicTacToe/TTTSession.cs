﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace TTT
{
    [Serializable]
    public class TTTSession
    {
        //Variables
        [SerializeField]
        private TTTGame _currentGame;
        private List<TTTGame> pastGames = new List<TTTGame>();
        private GameMode _gameMode;
        private int gamesWonByX, gamesWonByO;
        Difficulty _gameDifficulty;

        //Actions
        public Action<TTTGame> GameEnded, GameStarted;

        //Properties
        public TTTGame CurrentGame { get { return _currentGame; } set { _currentGame = value; } }


        //Constructor
        public TTTSession(GameMode gMode, Difficulty difficulty)
        {
            _gameMode = gMode;
            _gameDifficulty = difficulty;
        }

        public void NewGame()
        {
            if (_currentGame != null)
            {
                pastGames.Add(_currentGame);
            }
            _currentGame = new TTTGame(_gameMode, _gameDifficulty);
            _currentGame.GameEnded += EndGame;
            GameStarted(_currentGame);
        }

        public void EndGame()
        {
            gamesWonByX = _currentGame.GameState == GameState.XWon ? gamesWonByX + 1 : gamesWonByX;
            gamesWonByO = _currentGame.GameState == GameState.OWon ? gamesWonByO + 1 : gamesWonByO;

            GameEnded(_currentGame);
        }

        public void GetScore(out int xScore, out int oScore)
        {
            xScore = gamesWonByX;
            oScore = gamesWonByO;
        }
    }
}
