﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTT
{
    public class AIMove
    {
        public int score;
        public Vector2 move;

        public AIMove(int s, Vector2 m)
        {
            score = s;
            move = m;
        }
        public AIMove(int s)
        {
            score = s;
        }
        public AIMove() { }
    }
}