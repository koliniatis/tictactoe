﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TTT
{
    public class Tile : MonoBehaviour
    {
        //Variables
        [SerializeField]
        private TileStatus _curStatus;
        [SerializeField]
        private Sprite defaultSprite, xNormalSprite, oNormalSprite, xWonSprite, oWonSprite;
        [SerializeField]
        private Image img;
        [SerializeField]
        private Button button;
        [SerializeField]
        private TTTGame _currentGame;

        //Properties
        public TileStatus Status { get { return _curStatus; } set { _curStatus = value; } }
        public TTTGame CurrentGame { get { return _currentGame; } set { _currentGame = value; } }


        public void OnClick()
        {
            _curStatus = _currentGame.PlayerPlaying == Player.X_Player ? TileStatus.X_Touched : TileStatus.O_Touched;
            img.sprite = _curStatus == TileStatus.X_Touched ? xNormalSprite : oNormalSprite;
            button.interactable = false;
            _currentGame.MoveToNextRound();
        }

        public void ResetTile()
        {
            _curStatus = TileStatus.Empty;
            button.interactable = true;
            img.sprite = defaultSprite;
        }

        public void SetWinningSprite()
        {
            img.sprite = _curStatus == TileStatus.X_Touched ? xWonSprite : oWonSprite;
        }

        public void SetInteractable(bool b)
        {
            button.interactable = b;
        }
    }
}
