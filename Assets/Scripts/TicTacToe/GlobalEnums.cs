﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace TTT
{
    public enum ApplicationState { ChooseGameMode = 0, ChooseGameDifficulty = 1, Game = 2 }
    public enum GameState { Ongoing = 0, XWon = 1, OWon = 2, Draw = 3 }
    public enum Player { X_Player = 0, O_Player = 1 }
    public enum GameMode { PvP = 0, PvAI = 1 }
    public enum Difficulty { Easy = 0, Medium = 1, Hard = 2, Insane = 3 }
    public enum TileStatus { Empty = 0, X_Touched = -1, O_Touched = 1 }

}