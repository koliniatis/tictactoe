﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace TTT {
    public class GameManager : MonoBehaviour
    {
        //Variables
        public static GameManager Instance;
        [SerializeField]
        private Tile[] _tiles;

        [SerializeField]
        private TTTSession _curSession;
        private List<TTTSession> pastSessions = new List<TTTSession>();
        [SerializeField]
        private ApplicationState _appState;
        private GameMode curGameMode;

        [Space]
        [Header("Game Screens")]
        public GameObject ChooseGameMode;
        public GameObject ChooseDifficulty;
        public GameObject Game;

        //Actions
        public Action<TTTSession> OnSessionStarted, OnSessionEnded;

        //Properties
        public TTTSession CurrentSession { get { return _curSession; } set { _curSession = value; } }
        public ApplicationState ApplicationState { get { return _appState; } set { _appState = value; } }
        public Tile[] Tiles { get { return _tiles; } set { _tiles = value; } }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
            SetState(0);
        }

        public void ResetBoard()
        {
            foreach (Tile t in _tiles)
            {
                t.ResetTile();
            }
        }

        public void StartSession(int difficulty)
        {
            _curSession = new TTTSession(curGameMode, (Difficulty)difficulty);
            SetState(2);
            OnSessionStarted(_curSession);
            _curSession.NewGame();
        }

        public void EndSession()
        {
            OnSessionEnded(_curSession);
            pastSessions.Add(_curSession);
            SetState(0);
        }

        public void SetState(int stateId)
        {
            _appState = (ApplicationState)stateId;

            ChooseGameMode.SetActive(_appState == ApplicationState.ChooseGameMode);
            ChooseDifficulty.SetActive(_appState == ApplicationState.ChooseGameDifficulty);
            Game.SetActive(_appState == ApplicationState.Game);
        }

        public void SetGameMode(int gMode)
        {
            curGameMode = (GameMode)gMode;

            //No reason to select difficulty in local multiplayer, start the game right away
            if (curGameMode == GameMode.PvP)
            {
                StartSession(0);
            }
            else
            {
                SetState(1);
            }
        }

    }
}
