﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace TTT
{
    [Serializable]
    public class TTTGame
    {
        //Variables
        [SerializeField]
        private Player _playerPlaying;
        [SerializeField]
        private GameMode _gameMode;
        [SerializeField]
        private Difficulty _gameDifficulty;
        [SerializeField]
        private GameState _gameState;

        private Tile[,] tileBoard = new Tile[3, 3];

        //Actions
        public Action<Player> NewRound;
        public Action GameEnded;

        //Properties
        public Player PlayerPlaying { get { return _playerPlaying; } set { _playerPlaying = value; } }
        public GameMode GameMode { get { return _gameMode; } set { _gameMode = value; } }
        public GameState GameState { get { return _gameState; } set { _gameState = value; } }


        //Constructor
        public TTTGame(GameMode gMode, Difficulty difficulty)
        {
            _gameMode = gMode;
            _gameDifficulty = difficulty;
            _playerPlaying = _gameMode == GameMode.PvP ? (Player)UnityEngine.Random.Range(0, 2) : Player.X_Player;
            GameManager.Instance.ResetBoard();
            SetupTiles(GameManager.Instance.Tiles);

        }

        public void MoveToNextRound()
        {
            int[] tilesInWinningLine = new int[3];
            int[,] tileBoardWithInts = new int[3, 3];
            int winPlayer;

            //Convert Tile 2D Array to Int 2D Array to make things easier for certain functions
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    tileBoardWithInts[i, j] = (int)tileBoard[i, j].Status;
                }
            }

            //Check if the game has come to an end
            if (CheckIfSomeoneWon(tileBoardWithInts, out tilesInWinningLine, out winPlayer))
            {
                _gameState = winPlayer == -1 ? GameState.XWon : winPlayer == 1 ? GameState.OWon : GameState.Draw;

                if (winPlayer != 0)
                {
                    foreach (int t in tilesInWinningLine)
                    {
                        tileBoard[t / 3, t % 3].SetWinningSprite();
                    }
                }

                GameEnded();
                return;
            }
            _playerPlaying = _playerPlaying == Player.X_Player ? Player.O_Player : Player.X_Player;

            NewRound(_playerPlaying);

            //AI move if conditions are met
            if (PlayerPlaying == Player.O_Player && _gameMode == GameMode.PvAI)
            {
                SetInteractableOnEmptyTiles(false);
                PerformAIMove(tileBoardWithInts);
                SetInteractableOnEmptyTiles(true);
            }


        }

        #region Winning_Condition_Related
        public static bool CheckIfSomeoneWon(int[,] board, out int[] winTiles, out int winPlayer)
        {
            int[] tilesInWinningLine = new int[3];
            int winningStatus = 0;
            int emptyCells = 0;

            //Count empty cells
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    emptyCells = board[i, j] == 0 ? emptyCells + 1 : emptyCells;
                }
            }

            // Check columns
            for (var x = 0; x < 3; x++)
            {
                if (CheckLineForWinCondition(0, x, board, 1, 0, out tilesInWinningLine, out winningStatus))
                {
                    winTiles = tilesInWinningLine;
                    winPlayer = winningStatus;
                    return true;
                }
            }

            // Check rows
            for (var y = 0; y < 3; y++)
            {
                if (CheckLineForWinCondition(y, 0, board, 0, 1, out tilesInWinningLine, out winningStatus))
                {
                    winTiles = tilesInWinningLine;
                    winPlayer = winningStatus;
                    return true;
                }
            }
            // Check diagonals
            if (CheckLineForWinCondition(0, 0, board, 1, 1, out tilesInWinningLine, out winningStatus))
            {
                winTiles = tilesInWinningLine;
                winPlayer = winningStatus;
                return true;
            }

            if (CheckLineForWinCondition(0, 2, board, 1, -1, out tilesInWinningLine, out winningStatus))
            {
                winTiles = tilesInWinningLine;
                winPlayer = winningStatus;
                return true;
            }

            //If no one won, check for draw
            if (emptyCells == 0)
            {
                winTiles = tilesInWinningLine;
                winPlayer = 0;
                return true;
            }

            winTiles = tilesInWinningLine;
            winPlayer = winningStatus;

            return false;
        }

        public static bool CheckLineForWinCondition(int startX, int startY, int[,] board, int dx, int dy, out int[] winTiles, out int winPlayer)
        {
            //Initialize values
            int[] tilesInWinningLine = new int[3];
            int winningStatus = board[startX, startY];

            if (winningStatus == 0)
            {
                winTiles = tilesInWinningLine;
                winPlayer = winningStatus;
                return false;
            }

            //Check for three of the same
            for (int i = 0; i < 3; i++)
            {
                int x = startX + dx * i;
                int y = startY + dy * i;
                if (board[x, y] != winningStatus)
                {
                    winTiles = tilesInWinningLine;
                    winPlayer = winningStatus;
                    return false;
                }

                tilesInWinningLine[i] = x * 3 + y;
            }
            winTiles = tilesInWinningLine;
            winPlayer = winningStatus;
            return true;
        }
        #endregion

        #region AI_Move_Related

        public void PerformAIMove(int[,] board)
        {

            int randomNo = UnityEngine.Random.Range(0, 101);
            bool performBestMove = false;

            //Based on difficulty decide if the best move or a random move should be made
            switch (_gameDifficulty)
            {
                case Difficulty.Easy:
                    if (randomNo <= 25)
                    {
                        performBestMove = true;
                    }
                    break;
                case Difficulty.Medium:
                    if (randomNo <= 50)
                    {
                        performBestMove = true;
                    }
                    break;
                case Difficulty.Hard:
                    if (randomNo <= 75)
                    {
                        performBestMove = true;
                    }
                    break;
                case Difficulty.Insane:
                    if (randomNo <= 100)
                    {
                        performBestMove = true;
                    }
                    break;
            }

            if (performBestMove)
            {
                AIMove bestMove = GetBestMove(board, 1);
                tileBoard[(int)bestMove.move.x, (int)bestMove.move.y].OnClick();
            }
            else
            {
                PickRandomEmptyTile().OnClick();
            }
        }

        //MinMax implementation
        public AIMove GetBestMove(int[,] board, int player)
        {
            int[] tilesInWinningLine = new int[3];
            int winPlayer;

            //Check if the game has ended
            if (CheckIfSomeoneWon(board, out tilesInWinningLine, out winPlayer))
            {
                return new AIMove(10 * winPlayer);
            }

            List<AIMove> availableMoves = new List<AIMove>();

            //Recursively call the function and add store each move accompanied by how good of a move it is
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (board[x, y] == 0)
                    {
                        AIMove move = new AIMove();
                        move.move = new Vector2(x, y);
                        board[x, y] = player;
                        move.score = GetBestMove(board, -player).score;
                        availableMoves.Add(move);
                        board[x, y] = 0;
                    }
                }
            }


            //Choose which of the stored moves is the best
            int bestMoveCounter = 0;
            int bestScore = player == 1 ? -1000000 : 1000000;

            for (int i = 0; i < availableMoves.Count; i++)
            {
                bool isBetterMove = player == 1 ? availableMoves[i].score > bestScore : availableMoves[i].score < bestScore;
                if (isBetterMove)
                {
                    bestMoveCounter = i;
                    bestScore = availableMoves[i].score;
                }
            }

            return availableMoves[bestMoveCounter];
        }

        #endregion

        #region Tile_Related
        void SetupTiles(Tile[] tiles)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i].CurrentGame = this;
                tileBoard[i / 3, i % 3] = tiles[i];
            }
        }

        Tile PickRandomEmptyTile()
        {
            List<Tile> emptyTiles = new List<Tile>();

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (tileBoard[x, y].Status == TileStatus.Empty)
                    {
                        emptyTiles.Add(tileBoard[x, y]);
                    }
                }
            }

            int randomNo = UnityEngine.Random.Range(0, emptyTiles.Count);
            return emptyTiles[randomNo];
        }

        void SetInteractableOnEmptyTiles(bool b)
        {
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    if (tileBoard[x, y].Status == TileStatus.Empty)
                    {
                        tileBoard[x, y].SetInteractable(b);
                    }
                }
            }
        }
        #endregion
    }
}
